#!/bin/bash
echo "Stopping server..." &&
pm2 kill --name homepage &&
echo "Updating..." &&
git checkout -- . &&
git clean -df &&
git pull &&
echo "Updated. Installing dependencies..." &&
yarn install &&
echo "Installed. Making production build..."
yarn build &&
echo "Done." &&
pm2 start ./start.sh --name homepage
