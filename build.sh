#!/bin/bash
echo "Stopping server..." &&
pm2 kill &&
echo "Updating..." &&
git checkout -- . &&
git clean -df &&
git pull &&
echo "Updated. Making production build..." &&
yarn build &&
echo "Done." &&
echo "Removing ./public..." &&
rm -rf ./public &&
echo "Removed. Renaming ./build to ./public" &&
mv ./build ./public &&
echo "Renamed !" &&
pm2 start ./start.sh
