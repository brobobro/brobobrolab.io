import React from 'react';
import { Link } from "react-router-dom";

import { inject, observer } from 'mobx-react';

@inject('routing', 'navigation')
@observer

class NavListItem extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      activeClass: ''
    }
  }

  componentDidUpdate () {
    this.activeItem ()
  }

  activeItem () {
    let activeUrl = this.props.navigation.activePage,
        href = this.props.href;

    if (activeUrl === href) {
      return 'nav-list__link--active'
    }
    return ''
  }

  loadingHandler () {
    this.props.navigation.loading = true
  }

  render () {
    let it = this

    return (
      <li className="nav-list__item">
        <Link to={it.props.href}
          className={ "nav-list__link " + it.activeItem() }
          onClick={it.loadingHandler.bind(it)}>
          {it.props.text}
        </Link>
      </li>
    )
  }
}

export default NavListItem;
