import React from 'react';
import { Link } from "react-router-dom";

class Logo extends React.Component {
  // constructor() {
  //   super(props)
  // }

  render () {
    return (
      <Link className={"header-logo-wrap"} to={"/"}>
        <div className="header__logo-wrap">
          <figure className="header__logo header-logo">
            <span className="header-logo__bro">
              BRO
            </span>
            <span className="header-logo__bobro">
              bobro
            </span>
          </figure>
          {/* <img src={logo} alt="" className={"header__logo"} /> */}
        </div>
      </Link>
    )
  }
}

export default Logo;
