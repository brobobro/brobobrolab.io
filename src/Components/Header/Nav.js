import React from 'react';
import { inject, observer } from 'mobx-react';

import NavListItem from './NavListItem'

@inject('routing', 'navigation')
@observer

class Nav extends React.Component {
  // constructor() {
  //   super(props)
  // }
  burgerToggleHandler () {
    this.props.navigation.mobileNav = !this.props.navigation.mobileNav
  }

  render () {
    let it = this,
        navState = it.props.navigation.mobileNav
    return (
      <nav className="header__nav">
        <ul className={"header__nav-list nav-list " + (navState ? 'nav-list--active' : '')}>
          <NavListItem href='/'
                      text='Main' />
          <NavListItem href='/cv'
                      text='CV' />
          <NavListItem href='/about'
                      text='About' />
          <NavListItem href='/contact'
                      text='Contact' />
        </ul>
        <div className="header__burger-wrap">
          <button className={"header__burger burger " + (navState ? 'burger--active' : '')} onClick={it.burgerToggleHandler.bind(it)}>
            <span className="burger__eye burger__eye--left"></span>
            <span className="burger__eye burger__eye--right"></span>
            <span className="burger__mouth"></span>
          </button>
        </div>
      </nav>
    )
  }
}

export default Nav;
