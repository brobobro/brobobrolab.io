import React from 'react';
import Nav from './Nav'
import Logo from './Logo'
import './styles/scss/index.scss';

class Header extends React.Component {
  // constructor() {
  //   super(props)
  // }

  render () {
    return (
      <header className="header">
        <div className="header__inner">
          <Logo />
          <Nav />
        </div>
      </header>
    )
  }
}

export default Header;
