import React from 'react';
import Typing from 'react-typing-animation';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class TypeMainText extends React.Component {
  constructor (props) {
    super(props)

    this.dom = {}
  }

  finishType () {
    this.props.animations.title.step = 4
    this.props.cookies.set('mainAnimStep', 'done', { path: '/' });
  }

  render () {
    let it = this

    return (
      <article className="main-text">
        <Typing
          cursor={'|'}
          startDelay={10}
          speed={2}
          onFinishedTyping={it.finishType.bind(it)}>
          <p className="main-text__text">
            Here you'll find different information about me, my professional skills and the contact information.
          </p>
          <p className="main-text__text">
            This page was made for HRs, developers and for self-training, you can clone repo with the page, use for non-commercial deals and send me merge request.
          </p>
        </Typing>
      </article>
    )
  }
}

export default TypeMainText;
