import React from 'react';
import Cookies from 'universal-cookie';
import StaticHeading from './StaticHeading'
import TypeHeading from './TypeHeading'
import TypeMainDesc from './TypeMainDesc'
import MainDesc from './MainDesc'
import TypeMainText from './TypeMainText'
import MainText from './MainText'
import './styles/scss/index.scss'

import { inject, observer } from 'mobx-react';

const cookies = new Cookies();

@inject('animations')
@observer


class Main extends React.Component {
  headingController () {
    let loadedHeading = this.props.animations.title

    if (loadedHeading.step >= 1 || cookies.get('mainAnimStep') === 'done') {
      return (
        <StaticHeading />
      )
    } else {
      return (
        <TypeHeading />
      )
    }
  }

  descController () {
    if (this.props.animations.title.step === 2) {
      return <TypeMainDesc />
    } else if (this.props.animations.title.step > 2 || cookies.get('mainAnimStep') === 'done') {
      return <MainDesc />
    }
  }

  textController () {
    if (this.props.animations.title.step > 3 || cookies.get('mainAnimStep') === 'done') {
      return <MainText />
    } else if (this.props.animations.title.step === 3) {
      return <TypeMainText cookies={cookies}/>
    }
  }

  render () {
    return (
      <main className={"title-wrap " + ((this.props.animations.title.step > 1 || cookies.get('mainAnimStep') === 'done') ? "title-wrap--title-moved" : "")}>
        {this.headingController()}
        {this.descController()}
        {this.textController()}
      </main>
    )
  }
}

export default Main;
