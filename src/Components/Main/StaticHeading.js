import React from 'react';
import ReactDOM from 'react-dom';
import { TweenMax, Power3 } from 'gsap';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class StaticHeading extends React.Component {
  constructor (props) {
    super(props)

    this.dom = {}
  }

  componentDidMount () {
    if (this.props.animations.title.step === 1) {
      this.dom.root = ReactDOM.findDOMNode(this)

      let coords = {
        x: this.dom.root.offsetLeft - 20,
        y: this.dom.root.offsetTop - 40
      }

      this.animateTitle(this.dom.root, coords, this.clearTransform.bind(this))
    }
  }

  clearTransform () {
    this.props.animations.title.step = 2
    TweenMax.set(this.dom.root, {clearProps:"transform"});
  }

  animateTitle (block, coords, callback) {
    let { x, y } = coords

    return new TweenMax(block, 2, { ease: Power3.easeOut, x: -x, y: -y, onComplete: callback}).delay(.5);
  }

  render () {
    return (
      <section className={"title title--typed"}>
        <h3 className="title__supheading">hi! my name is</h3>
        <h1 className="title__heading">Bro Bobro</h1>
      </section>
    )
  }
}

export default StaticHeading;
