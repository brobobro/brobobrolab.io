import React from 'react';
import Typing from 'react-typing-animation';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class TypeMainDesc extends React.Component {
  constructor (props) {
    super(props)

    this.dom = {}
  }

  finishType () {
    this.props.animations.title.step = 3
  }

  render () {
    let it = this
    return (
      <article className="main-desc">
        {/* <small className="main-desc__aka">aka</small> */}
        <Typing
          cursor={'|'}
          startDelay={1}
          speed={2}
          onFinishedTyping={it.finishType.bind(it)}>
          <h2 className="main-desc__heading">
            Andrew Bobrov
          </h2>
          <p className="main-desc__caption">
            Frontend developer
          </p>
        </Typing>
      </article>
    )
  }
}

export default TypeMainDesc;
