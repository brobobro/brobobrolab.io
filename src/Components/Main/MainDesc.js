import React from 'react';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class MainDesc extends React.Component {
  render () {
    return (
      <article className="main-desc">
        <h2 className="main-desc__heading">
          Andrew Bobrov
        </h2>
        <p className="main-desc__caption">
          Frontend developer
        </p>
      </article>
    )
  }
}

export default MainDesc;
