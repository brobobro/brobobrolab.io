import React from 'react';
import Typing from 'react-typing-animation';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class TypeHeading extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      finished: false
    }
  }

  finishType () {
    this.props.animations.title.step = 1
  }

  render () {
    const it = this
    return (
      <section className={"title " + (it.props.animations.title.typed ? 'title--typed' : '')}>
        <Typing
          cursor={'|'}
          startDelay={500}
          onFinishedTyping={it.finishType.bind(it)}
          speed={100}>
          <h3 className="title__supheading">hi! my name is</h3>
          <h1 className="title__heading">Bro Bobro</h1>
        </Typing>
      </section>
    )
  }
}

export default TypeHeading;
