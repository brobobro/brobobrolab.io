import React from 'react';

import { inject, observer } from 'mobx-react';

@inject('animations')
@observer
class MainText extends React.Component {
  constructor (props) {
    super(props)

    this.dom = {}
  }

  render () {
    return (
      <article className="main-text">
        <p className="main-text__text">
          Here you'll find different information about me, my professional skills and the contact information.
        </p>
        <p className="main-text__text">
          This page was made for HRs, developers and for self-training, you can clone repo with the page, use for non-commercial deals and send me merge request.
        </p>
      </article>
    )
  }
}

export default MainText;
