import React from 'react';
import { inject, observer } from 'mobx-react';
import { H01 } from '../Common/Typografy.js';
import Work from './Work.js';
import Skills from './Skills.js';
import workData from './data/work.js';
import './styles/scss/index.scss'

@inject('routing', 'navigation')
@observer

class Cv extends React.Component {
  componentDidMount () {
    // window.addEventListener('load', () => {
      this.props.navigation.loading = false
    // });
  }

  render () {
    return (
      <main className="page-content">
        <div className="page-content__inner">
          <section className="cv">
            <H01>CV</H01>
            <div className="cv__skills">
              <Skills skills={workData.skills} />
            </div>
            <div className="cv__experience">
              <Work work={workData.experience} />
            </div>
          </section>
        </div>
      </main>
    )
  }
}

export default Cv;
