import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('routing', 'navigation')
@observer

class SkillsItem extends React.Component {

  render () {
    let skills = this.props.skillItem

    return (
      <figure className="skills-item">
        <ul className="skills-item__list">
          {skills.map((skill, s, skills) => {
            return (
              <li className="skills-item__list-item" key={`skill_${s}`}>
                {skill}
              </li>
            )
          })}
        </ul>
      </figure>
    )
  }
}

export default SkillsItem;
