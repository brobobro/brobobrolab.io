import React from 'react';
import { inject, observer } from 'mobx-react';
import WorkItem from './WorkItem.js';

@inject('routing', 'navigation')
@observer

class Work extends React.Component {

  render () {
    let works = this.props.work

    return (
      <article className="work">
        <h3>Work experience</h3>
        {works.map((work, w, works) => {
          return (
            <WorkItem work={work} key={`work_item_${w}`}/>
          )
        })}
      </article>
    )
  }
}

export default Work;
