import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('routing', 'navigation')
@observer

class WorkItem extends React.Component {
  endDateController (date) {
    if (date) {
      return date
    } else {
      return 'present'
    }
  }

  siteHandler (work) {
    if (work.site) {
      return (
        <a href={work.site} target="_blank" className="work-item__site">{work.company}</a>
      )
    } else {
      return (
        <h5 className="work-item__company">
          { work.company }
        </h5>
      )
    }
  }

  render () {
    let it = this,
        work = it.props.work

    return (
      <figure className="work-item">
        <div className="work-item__dates">
          {`${work.start} - ` + (it.endDateController.call(it, work.end))}
        </div>
        {it.siteHandler.call(it, work)}
        <h6 className="work-item__position">
          {work.position}
        </h6>
        <ul className="work-item__list">
          {work.functions.map((fun, f, functions) => {
            return (
              <li className="work-item__list-item" key={`work_list_item_${f}`}>
                {fun}
              </li>
            )
          })}
        </ul>
      </figure>
    )
  }
}

export default WorkItem;
