export default {
  experience: [
    {
      start: '11.2016',
      end: false,
      company: 'Uchi.ru',
      site: 'https://uchi.ru',
      position: 'frontend developer/main frontend developer (site)',
      functions: [
        'html',
        'css',
        'react components',
        'gui development',
        'structure development',
        'conducting interviews',
        'training employees',
        'working in communication with backend developers, managers, designers'
      ],
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In necessitatibus, perspiciatis tempore unde similique corporis cumque rerum nemo. Vitae aliquam harum illo qui, voluptates aperiam magnam non voluptatum architecto quo?'
    },
    {
      start: '09.2016',
      end: '11.2016',
      company: 'AMO CRM',
      site: 'https://www.amocrm.ru/',
      position: 'frontend developer',
      functions: [
        'html',
        'css',
        'backbone.js',
        'working in communication with backend developers, managers, designers'
      ],
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In necessitatibus, perspiciatis tempore unde similique corporis cumque rerum nemo. Vitae aliquam harum illo qui, voluptates aperiam magnam non voluptatum architecto quo?'
    },
    {
      start: '11.2015',
      end: '12.2015',
      company: '8 Bit Group',
      site: 'http://8bitgroup.ru/',
      position: 'frontend developer',
      functions: [
        'html',
        'css',
        'page localization',
        'working in communication with backend developers, managers, designers'
      ],
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In necessitatibus, perspiciatis tempore unde similique corporis cumque rerum nemo. Vitae aliquam harum illo qui, voluptates aperiam magnam non voluptatum architecto quo?'
    },
    {
      start: '03.2014',
      end: false,
      company: 'freelance',
      position: 'frontend developer',
      functions: [
        'html',
        'css',
        'developing and customizing js plugins',
        'support wordpress sites',
        'working in communication with backend developers, managers, designers'
      ],
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In necessitatibus, perspiciatis tempore unde similique corporis cumque rerum nemo. Vitae aliquam harum illo qui, voluptates aperiam magnam non voluptatum architecto quo?'
    },
    {
      start: '11.2013',
      end: '02.2014',
      company: 'OOO "FOBOS"',
      position: 'support and introduction specialist',
      functions: [
        'employees support',
        'employees training'
      ],
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. In necessitatibus, perspiciatis tempore unde similique corporis cumque rerum nemo. Vitae aliquam harum illo qui, voluptates aperiam magnam non voluptatum architecto quo?'
    }
  ],
  skills: [
    [
      'html',
      'template engines (pug, slim, erb)',
      'css',
      'preprocessors (scss, stylus, less)',
      'svg'
    ],
    [
      'vanillajs',
      'jquery',
      'react',
      'mobx',
      'keystonejs (frontend + backend)'
    ],
    [
      'using and setting up gulp',
      'using webpack',
      'experience with projects on: ruby on rails, php, nodejs'
    ],
    [
      'git',
      'terminal',
      'BEM',
      'experience with large projects',
      'developing styleguides',
      'working in team',
      'conducting interviews',
      'training new employees'
    ],
    [
      'sketch (figma, zeppelin)',
      'adobe photoshop'
    ]
  ],
  expectations: {

  }
}
