import React from 'react';
import { inject, observer } from 'mobx-react';
import SkillsItem from './SkillsItem.js';

@inject('routing', 'navigation')
@observer

class Skills extends React.Component {
  render () {
    let skills = this.props.skills
    
    return (
      <aside className="skills">
        <h3>Skills</h3>
        {skills.map((skill, s, skills) => {
          return (
            <SkillsItem skillItem={skill} key={`skill_pack_${s}`}/>
          )
        })}
      </aside>
    )
  }
}

export default Skills;
