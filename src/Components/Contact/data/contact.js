export default [
  // {
  //   field: 'linkedIn',
  //   value: 'www.linkedin.com/in/andrew-bro-bobro',
  //   linkAttribute: '',
  //   newTab: true
  // },
  {
    field: 'gitlab',
    value: 'https://gitlab.com/brobobro',
    linkAttribute: '',
    newTab: true
  },
  {
    field: 'telegram',
    value: 'https://t.me/brobobro',
    linkAttribute: '',
    newTab: true
  },
  {
    field: 'facebook',
    value: 'https://fb.com/frontendordie',
    linkAttribute: '',
    newTab: true
  },
  {
    field: 'email',
    value: 'andycyberries@gmail.com',
    linkAttribute: 'mailto:',
    newTab: false
  },
]
