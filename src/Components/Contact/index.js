import React from 'react';
import { inject, observer } from 'mobx-react';
import { H01 } from '../Common/Typografy.js';
import contacts from './data/contact'
import './styles/scss/index.scss'

@inject('routing', 'navigation')
@observer

class Contact extends React.Component {

  componentDidMount () {
    this.props.navigation.loading = false
  }

  newTabController (contact) {
    if (contact.newTab) {
      return '_blank'
    }
  }

  render () {
    let it = this

    return (
      <main className="page-content">
        <div className="page-content__inner">
          <H01>Contact me</H01>
          <table className={'contact-table'}>
            <tbody>
              {contacts.map((contact, i, contacts) =>
                <tr key={i}>
                  <td>{contact.field}</td>
                  <td>
                    <a href={contact.linkAttribute + contact.value} target={it.newTabController.call(it, contact)}>{contact.value}</a>
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </main>
    )
  }
}

export default Contact;
