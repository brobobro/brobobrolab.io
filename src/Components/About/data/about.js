import moment from 'moment';

const birthday = moment().diff('1989-03-10', 'years',false);
const exp = moment().diff('2014-05-01', 'years',false);

export default {
  aboutShort: [
    {
      field: 'name',
      value: 'Andrew Bobrov'
    },
    {
      field: 'age',
      value: `${birthday} years`
    },
    {
      field: 'experience',
      value: `${exp} years`
    }
  ],
  aboutText: [
    'Hello! Welcome to my homepage!',
    'I am a frontend developer from Moscow, Russia. I\'m working in education platform <a href="https://uchi.ru" target="_blank">uchi.ru</a> in olympiad departament. Our departament is creating challenges for children in school subjects (maths, russian language, english language and more).',
    'In my free time I love traveling, camping and hiking, going to music festivals and gigs, riding bicycle. I appreciate in people creativity, free mind, sense of humour and love for cats.',
    'Now I\'m in search of new job, relocation possible.'
  ]
}
