import React from 'react';
import { inject, observer } from 'mobx-react';
import { Pixelify } from "react-pixelify";
import { H01 } from '../Common/Typografy.js';
import about from './data/about';
import './styles/scss/index.scss'

const src = require("./images/myself.jpg");

@inject('routing', 'navigation')
@observer

class About extends React.Component {

  componentDidMount () {
    this.props.navigation.loading = false
  }

  // constructor() {
  //   super(props)
  // }
  render () {
    return (
      <main className="page-content">
        <div className="page-content__inner">
          <H01>
            About
          </H01>
          <div className="about">
            <div className="about__photo-wrap">
              <div className="about__photo">
                <Pixelify
                  src={src}
                  pixelSize={10}
                  className={"about__canvas"}
                />
                <img src={src} alt="avatar" className="about__avatar"/>
              </div>
            </div>
            <div className="about__desc">
              <table className={'about__table about-table'}>
                <tbody>
                  {about.aboutShort.map((a, i, about) =>
                    <tr key={i}>
                      <td>{a.field}</td>
                      <td>
                        {a.value}
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
            <article className="about__article">
              {about.aboutText.map((t, i, aboutText) =>
                <p className="about__text" key={i} dangerouslySetInnerHTML={{ __html: t }}></p>
              )}
            </article>
          </div>
        </div>
      </main>
    )
  }
}

export default About;
