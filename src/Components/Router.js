import React, { Component } from 'react';
import { Router, Route, Switch } from "react-router-dom";
import { observer, inject } from 'mobx-react';
// import { Loader } from 'react-loaders';
// import DevTools from 'mobx-react-devtools';
// https://github.com/trungdq88/react-router-page-transition/blob/master/examples/reveal/js/App.jsx

import Header from './Header';
import Main from './Main';
import Cv from './Cv';
import About from './About';
import Contact from './Contact';
import RandomCat from './RandomCat';
import NoMatch from './NoMatch';

@inject('animations', 'navigation')
@observer

class AppRouter extends Component {

  componentDidMount () {
    let it = this
    it.props.animations.monitor.loadStep = 'loading'
    window.addEventListener('load', () => {
      it.props.animations.monitor.loadStep = 'loaded'
    });
  }

  componentDidUpdate () {

  }

  loadedClassHandler (klass) {
    let it = this,
        loadStep = it.props.animations.monitor.loadStep

    switch (loadStep) {
      case 'loading':
        return klass + '--loading'
      case 'loaded':
        return klass + '--loaded'
      default:
        return klass
    }
  }

  overlayTransitionEndHandler () {

  }

  render() {
    let it = this
    // (this.props.animations.title.moved ? 'app-wrapper__inner--loaded' : '')
    return (
      <Router history={this.props.history}>
        <div className={"app-wrapper " + (it.loadedClassHandler.call(it, 'app-wrapper'))}>
          <Header />
          {/* <Loader type="pacman" active={it.props.navigation.loading} /> */}
          <div className={"app-wrapper__inner "}>
            <Switch>
              <Route exact path="/" component={Main} />
              <Route path="/cv" component={Cv} />
              <Route path="/about" component={About} />
              <Route path="/contact" component={Contact} />
              <Route path="/randomcat" component={RandomCat} />
              <Route component={NoMatch} />
            </Switch>
          </div>
          <footer className="footer">Andrew Bobrov, {(new Date()).getFullYear()}</footer>
          {/* <DevTools /> */}
        </div>
      </Router>
    );
  }
}

export default AppRouter;
