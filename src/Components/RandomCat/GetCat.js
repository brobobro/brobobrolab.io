import React from 'react';
import axios from 'axios';
import { Loader } from 'react-loaders';

class GetCat extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      catState: true,
      catUrl: ''
    }
  }

  componentDidMount () {
    this.getCatHandler()
  }

  getCatHandler () {
    let it = this

    it.setState({
      catState: true
    })

    axios({
      method: 'get',
      url: 'https://api.thecatapi.com/v1/images/search?mime_type=jpg,png',
      headers: {'x-api-key': 'ca301683-ffaf-4883-afb2-49947a9061b6'}
    })
      .then((response) => {
        it.setState({
          catUrl: response.data[0].url
        })
        it.rendered.bind(it)
      })
      .catch((err) => {
        console.log(err);
        it.getCatHandler()
      })
  }

  loaded () {
    this.setState({
      catState: false
    })
  }

  startRender () {
    let _it = this
    requestAnimationFrame(this.loaded.bind(_it));
  }

  rendered () {
    let _it = this
    requestAnimationFrame(_it.startRender.bind(_it));
  }

  catGen () {
    let it = this;
    return (
      <img src={it.state.catUrl} alt="" className="get-cat__img" onLoad={it.rendered.bind(it)} />
    )
  }

  render () {
    let it = this

    return (
      <div className="get-cat">
        <Loader type="pacman" active={this.state.catState} />
        <div className="get-cat__img-wrap">
          {it.catGen()}
        </div>
        <button className="get-cat__get" onClick={it.getCatHandler.bind(it)}>GET A CAT !</button>
      </div>
    )
  }
}

export default GetCat;
