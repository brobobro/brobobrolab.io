import React from 'react';
import { H01 } from '../Common/Typografy.js';
import GetCat from './GetCat.js';
import './styles/scss/index.scss'

class RandomCat extends React.Component {
  render () {
    return (
      <main className="page-content">
        <div className="page-content__inner">
          <section className="cats">
            <H01>Cats</H01>
            <GetCat />
          </section>
        </div>
      </main>
    )
  }
}

export default RandomCat;
