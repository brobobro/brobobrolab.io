import React from 'react';
import './styles/scss/index.scss'

export class H01 extends React.Component {
  render () {
    return (
      <h1 className="h01">
        <span className="h01__span">
          {this.props.children}
        </span>
        <span className="h01__angle"></span>
      </h1>
    )
  }
}
