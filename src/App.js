import React, { Component } from 'react';
import createBrowserHistory from 'history/createBrowserHistory';
import { decorate, observable } from "mobx";
import { Provider } from 'mobx-react';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import AppRouter from './Components/Router';

const browserHistory = createBrowserHistory();
const routingStore = new RouterStore();

const stores = {
  // Key can be whatever you want
  routing: routingStore,
  navigation: {
    activePage: '/',
    mobileNav: false,
    loading: false
  },
  animations: {
    title: {
      step: 0,
      typed: false,
      moved: false,
      descTyped: false
    },
    monitor: {
      loadStep: 'unloaded'
    }
  },
  cv: {},
  dom: {
    titleWrap: null
  }
};

decorate(stores, {
  routing: observable,
  navigation: observable,
  animations: observable
})

const history = syncHistoryWithStore(browserHistory, routingStore);

// eslint-disable-next-line
const unsubscribeFromStore = history.subscribe((location, action) => {
  stores.navigation.activePage = location.pathname
  window.scrollTo(0,0)
  stores.navigation.mobileNav = false
});

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      loadStep: 'unloaded',
    }
  }

  componentDidUpdate () {
    stores.navigation.loading = false
  }

  componentDidMount () {
    // window.addEventListener('load', () => {
      // console.log('onload');
      stores.navigation.loading = false
    // });
  }

  shouldComponentUpdate() {

  }

  loadedClassHandler (klass) {
    let it = this
    if (it.props.animations.monitor.loadStep === 'loaded') {
      return klass + '--loaded'
    }
  }

  overlayTransitionEndHandler () {

  }

  render() {
    return (
      <Provider {...stores}>
        <AppRouter history={history} />
      </Provider>
    );
  }
}

export default App;
