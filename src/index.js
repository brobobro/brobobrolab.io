import React from 'react';
import ReactDOM from 'react-dom';
import 'typeface-press-start-2p';
import './styles/index.scss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
