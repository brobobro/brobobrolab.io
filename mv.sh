#!/bin/bash
echo "Removing ./public..." &&
rm -rf ./public &&
echo "Removed. Renaming ./build to ./public" &&
mv ./build ./public &&
echo "Renamed!"
